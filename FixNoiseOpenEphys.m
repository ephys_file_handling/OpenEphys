function []=FixNoiseOpenEphys (direct);

% this is supposed to remove movement artefacts from rachel's recording
% i.e. mice that are head restrained and push, which gets high freauency 
% (>120 Hz and as high as 12Hz)
% it should make new files without noise in a subdirectory called
% 'NoArtefacts'

if nargin <1
direct='MyRecordingPath'; %please change that

end
cd (direct);
% deadChans={'4','5'};
% deadChans={'25'};


deadChans={'10'}; % #### important!!!! If you have dead channels

OrderEdge=1:64; % there you have 64 channels feel free to add more


% run 5 minutes by 5 minutes
 NAM=['100_',num2str(OrderEdge(1),'%d'),'.continuous'];
        [~,t, header] = load_open_ephys_data_faster(NAM);

SF=header.header.sampleRate;
 wn=[100]./(SF/2);
 [b,a]=butter(2,wn,'high');


t=t-t(1);
Allchans=zeros(length(OrderEdge),length(t));
Allchansf=Allchans;

bad=zeros(length(OrderEdge),1);

headers=struct('info',[]);

    for ch=1:length(OrderEdge)
        
        for target=1:length(deadChans)
            if strcmp(deadChans(target), num2str(OrderEdge(ch),'%d'))
                bad(ch,1)=1;
            end
        end
        
        disp(['Ch', num2str(ch)])
        if ~bad(ch,1)
            
            NAM=['100_',num2str(OrderEdge(ch),'%d'),'.continuous'];
%             [t2, dtmp, info] = read_cont_PP(NAM,start,stop);
            [dtmp,t2,info] = load_open_ephys_data_faster(NAM);
            t2=t2-t2(1);
            
            headers(ch).info=info;
            dtmp=dtmp./info.header.bitVolts; % I should not convert it
%            since I will save it again afterwards
           
            if length(dtmp)>size(Allchans,2)
                dtmp(size(Allchans,2)+1:end)=[];
                t2(size(Allchans,2)+1:end)=[];
            end
            if length(dtmp)<size(Allchans,2)
                dtmp=[dtmp zeros(size(Allchans,2)-length(t2))];
                t2=[t2 zeros(size(Allchans,2)-length(t2))];
            end
            dtmp2=filtfilt(b,a,dtmp);
            Allchans(ch,:)=dtmp;
            Allchansf(ch,:)=dtmp2;
        else
           [~,~,info] = load_open_ephys_data_faster(NAM);
             headers(ch).info=info;
            Allchans(ch,:)=zeros(size(dtmp));
             Allchansf(ch,:)=zeros(size(dtmp));
        end
%         bad(ch,1)=0;
    end
%     SF=SF/10;
    
%     bigmn=sum(Allchans);
%     
%      [icasig, A, W] = fastica (Allchans, 'numOfIC' ,4);
%      Allchans2=Allchans;
%      
disp('Removing Movement artefacts')
       for ff=1:size(Allchans,1)
           if bad(ch,1)
               Allchans(ff,:)=zeros(size(Allchans(ff,:)));
           else
           Allchans(ff,:)=Allchans(ff,:)- mean(Allchansf,1);
           end
       end
       
       clear Allchansf
       disp('Writing new files to disk')
       % now write it in .continuous files? easy if dat file only
       mkdir('Artefactfree')
      
         cd ('Artefactfree')
       for ch=1:length(OrderEdge)
         
           disp(['Ch', num2str(ch)])
           fName=['100_',num2str(OrderEdge(ch),'%d'),'.continuous'];
           write_New_Continuous (fName,Allchans(ch,:),headers(ch).info);
       end
       
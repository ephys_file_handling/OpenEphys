
function []= writeContinuous (fName,lfp,info)

% writes a new open-ephys .continuous file based on the lfp I give as input
% and info data that I got from the orinal file
% I created a structure for each file I read with 
% : the header
% Each continuous channel within each processor has its own file, titled "XXX_CHY.continuous", where XXX = the processor ID #, and Y = the channel number. For each record, it saves:
% 
% One little-endian int64 timestamp (actually a sample number; this can be converted to seconds using the sampleRate variable in the header)
% One little-endian uint16 number (N) indicating the samples per record (always 1024, at least for now)
% One little-endian uint16 recording number (version 0.2 and higher)
% 1024 big-endian int16 samples
% 10-byte record marker (0 1 2 3 4 5 6 7 8 255)
% This makes up a record of 2070 bytes (= 8 + 2 + 2 + 2048 + 10).
% blockBytes=[8 2 2 2048 10]; % for continuous files should be like this
% gather the data 
% header has 1024 bytes too but 'char*1'
% you have to be in the new folder called 'Artefactfree'
% Pierre-Pascal Lenck-Santini
% INSERM, INMED Marseille, France

cd ..
fid = fopen(fName);

% prepare header
NUM_HEADER_BYTES = 1024;
fseek(fid,0,'bof');
hdr = fread(fid, NUM_HEADER_BYTES, 'char');
fclose (fid);
% 

cd ('artefactfree')
fidout=fopen(fName,'w');
fprintf(fidout,'%s',char(hdr)); % print header
chunkdatasz=(8 + 2 + 2 + 2048 + 10);

% Time stamps
fwrite(fidout,info.ts(1), 'uint64','l');
fwrite(fidout,uint64(info.ts(2:end)), 'uint64',chunkdatasz-8,'l');


% samples per record
numr=ones(size(info.recNum))*1024;
fseek(fidout,NUM_HEADER_BYTES+8 ,'bof');
fwrite(fidout,int16(numr(1)),'uint16','l');
fwrite(fidout,int16(numr(2:end)),'uint16',chunkdatasz-2,'l');

% record number % dunno why it's zero for all

fseek(fidout,NUM_HEADER_BYTES+8+2,'bof');
fwrite(fidout,int16(info.recNum(1)),'uint16','l');
fwrite(fidout,int16(info.recNum(1)),'uint16',chunkdatasz-2,'l');

%lfpData 
fseek(fidout,NUM_HEADER_BYTES+8+2+2,'bof');
fwrite(fidout, int16(lfp(1:1024)), '1024*int16','b');
fwrite(fidout, int16(lfp(1025:end)), '1024*int16',22,'b');

record_marker= [0 1 2 3 4 5 6 7 8 255];
record_marker2= repmat(record_marker,1,length(info.recNum));
fseek(fidout,NUM_HEADER_BYTES+8+2+2+2048,'bof');

fwrite(fidout, uint8(record_marker),'10* uint8'); 
fwrite(fidout, uint8(record_marker2(11:end)),'10* uint8',chunkdatasz-10); 

fclose(fidout);


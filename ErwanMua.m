function []= ErwanMua ()

% Order=[31 27 22 18 28 23 21 26 29 24 20 25 30 19 32 17 1 16 3 14 9 10 8 2 7 15 11 12 6 13 5 4]; %if CH
Order=[1:32]; % if no Ch

[fname, pathname,findex] = uigetfile('*.continuous', 'Please select a continuous file');

cd (pathname)

prefix='100_';
%prefix='100_CH';
Wn=[300]./(30000/2);


UnitData=struct ('Times', []);

for ch=1:length(Order)

    disp(['treating chnanel ', num2str(Order(ch),'%d')])
    NAM=[prefix,num2str(Order(ch),'%d'),'.continuous'];
    [dtmp, time, header, ~] = fct_read_continuous_file(NAM);
    
    
  
    [b,a]=cheby2(3,20, Wn,'high');
    dtmp=filtfilt(b,a,dtmp);
    [b,a]=cheby2(5,20, Wn,'high');
    dtmp=filtfilt(b,a,dtmp);
    
    edges=linspace(-100, 100,1000);
    [mu,sigma]=normfit(dtmp);
   
    thresh=4.5*(sigma/sqrt(2)); %I could use the std but it's close to the same and does not tale the noise into account

    [mins]=LocalMinima(dtmp,30,thresh*-1);
    [maxs]=LocalMinima(dtmp*-1,30,thresh*-1);
    discard=zeros(size(mins));

    for sp=1:length(mins)
        a=find(maxs>=mins(sp)-15 & maxs<=mins(sp)+15);
         if length(a)>0
            discard(sp)=1;
         end
    
    end
    mins(find(discard==1))=[];
    
    UnitData.Times=[UnitData.Times; mins];
    
end

time=time-time(1);
UTimes=unique(UnitData.Times);
[aa,bb]=histc(UnitData.Times,UTimes);
UTimes(find(aa>5))=[];

MUATimes=time(UTimes);

edg2=[time(1): 0.5: time(end)];
mnvect=histc(MUATimes, edg2)*(1/0.5);
mnvect(end)=[];
edg2(end)=[];

figure
plot(edg2,mnvect/length(Order),'k')

save('MUATimes.mat','MUATimes','mnvect','edg2')





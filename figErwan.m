cd ('D:\ERWAN\MUAdetections')

mousenum=[0 1 5 6]; % here I took only 4 mice, the others had too many artefacts

MP=['k','r','b'];
BIG=[];
 NAM=['Mouse-',num2str(mousenum(4),'%d'),'-MUATimes.mat'];
   load(NAM) % just to get the variable edg2
    
    refedg=edg2;
    
for ind=1:length(mousenum)
    NAM=['Mouse-',num2str(mousenum(ind),'%d'),'-MUATimes.mat'];
    load(NAM)
    %
    subplot(2,2,ind)
   
    mnvect=smooth(mnvect,60);
    mnvect=mnvect./nanmean(mnvect);%10 minutes by 60 seconds by2 becquse 2 bins per second: mnvect(1:5*60*2)
    
    mnvect(find(mnvect<0))=0;
    edg2=[0: 0.5: (length(mnvect)*0.5)];
    edg2(end)=[];
    
    plot(edg2./60,mnvect,'k')
    %     plot(edg2./60,mnvect,MP(ind))
    %     hold on
    title(['Mouse-',num2str(mousenum(ind),'%d')])
    xlabel('Time(min)')
    ylabel('MUA (Normalized Units/s)')
    tmp=interp1(edg2,mnvect,refedg);
    BIG=[BIG; tmp];
end

xlim([0 60])

figure
subplot(2,1,1)
plot(edg2./60,mean(BIG),'k')

subplot(2,1,2)
plot(edg2./60,median(BIG),'k')

cd ('D:\ERWAN\MUAdetections')
save('Global.mat', 'BIG', 'refedg')
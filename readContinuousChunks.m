function [timestamps,samples,info] = readContinuousChunks (filename,startT,stopT)
% [timestamps,samples] = read_cont_PP (filename,startT,stopT);
% to read continuous files pieces by pieces
% timestamps in seconds, samples are in microvolts
 % startT should be in seconds, consider first time stamp = 0s though.
 % for example if recording started at 240s, sartT=1 means you start at
 % 241s and on
 
 
 % There is a big issue if, when recording you click two times record/ stop 
 % On the  same file. This program expects timestamps to be continuously 
 % acquired in order to cut chunks at expected times
 % So if the file is recorded with interruptions in between, the code will
 % not work
 % Pierre-Pascal Lenck-Santini adapted from the openephys github (python
 % codes from Josh Siegle
 
 
[~,~,filetype] = fileparts(filename);
if ~any(strcmp(filetype,{'.continuous'}))
    error('File extension not recognized. Please use a ''.continuous'' file.');
end


fid = fopen(filename);
fseek(fid,0,'eof');
filesize = ftell(fid);

NUM_HEADER_BYTES = 1024;
fseek(fid,0,'bof');
hdr = fread(fid, NUM_HEADER_BYTES, 'char*1');
info = getHeader(hdr);

SF=info.header.sampleRate;
if isfield(info.header, 'version')
    version = info.header.version;
else
    version = 0.0;
end
%%%%%%%%%%%%% critical!!!  file format
SAMPLES_PER_RECORD = 1024; % data is chunked by 1024samples (each is an int16)
bStr = {'ts' 'nsamples' 'recNum' 'data' 'recordMarker'};
bTypes = {'int64' 'uint16' 'uint16' 'int16' 'uint8'};
bRepeat = {1 1 1 SAMPLES_PER_RECORD 10};

dblock = struct('Repeat',bRepeat,'Types', bTypes,'Str',bStr);



if version < 0.2, dblock(3) = []; end
if version < 0.1, dblock(1).Types = 'uint64'; dblock(2).Types = 'int16'; end


blockBytes = str2double(regexp({dblock.Types},'\d{1,2}$','match', 'once')) ./8 .* cell2mat({dblock.Repeat});
% blockBytes=[8 2 2 2048 10]; % for continuous files should be like this
numIdxAll = floor((filesize - NUM_HEADER_BYTES)/sum(blockBytes)); % number of data segments (each is 1024*int16 points

SegDur=1024./SF; % duration of one datasegment

StartSegT=floor(startT/SegDur);
StopSegT=ceil(stopT/SegDur);
startSeg=StartSegT*sum(blockBytes); % time in bytes
Blocs2read=StopSegT-StartSegT;

numIdx= Blocs2read;
info.ts=segRead('ts');
samples=segRead('data','b').* info.header.bitVolts;

fseek(fid, sum(blockBytes(1:1-1))+NUM_HEADER_BYTES, 'bof'); % get first timestamp
 time1 = fread(fid, 1*dblock(1).Repeat, sprintf('%d*%s', ...
        dblock(1).Repeat,dblock(1).Types));
    time1=time1./info.header.sampleRate;


 info.nsamples = segRead('nsamples');
if nargout>1 % do not create timestamp arrays unless they are requested
    timestamps = nan(size(samples));
    current_sample = 0;
    for record = 1:length(info.ts)
        timestamps(current_sample+1:current_sample+info.nsamples(record)) = info.ts(record):info.ts(record)+info.nsamples(record)-1; %% info.ts has strange value
        current_sample = current_sample + info.nsamples(record);
    end
    timestamps = timestamps./info.header.sampleRate;
end

    
cutbef=find(timestamps<startT+time1); % cut the time stampes before T1
timestamps(cutbef)=[];
samples(cutbef)=[];

cutaft=find(timestamps>(stopT+time1)); % cut the time stampes before T1
timestamps(cutaft)=[];
samples(cutaft)=[];



function seg = segRead(segName,mf)
    if nargin == 1, mf = 'l'; end % big endian blabla
    segNum = find(strcmp({dblock.Str},segName));
    fseek(fid, startSeg+sum(blockBytes(1:segNum-1))+NUM_HEADER_BYTES, 'bof'); 
%     fseek(fid, sum(blockBytes(1:segNum-1))+NUM_HEADER_BYTES, 'bof'); 
     seg = fread(fid, numIdx*dblock(segNum).Repeat, sprintf('%d*%s', ...
        dblock(segNum).Repeat,dblock(segNum).Types), sum(blockBytes) - blockBytes(segNum), mf);
end
 fclose (fid)  ;   
end
function info = getHeader(hdr)
eval(char(hdr'));
info.header = header;
end
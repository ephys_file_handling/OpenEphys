## OpenEphys File Management
Pierre-Pascal Lenck-Santini, Ph.D.
Inserm, INMED AMU U1249

## Description
These are matlab .m files that I use to 
1) load only chunks of .continuous datafiles (readContinuousChunks) and 
2) write a .continuous file from an existing lfp dataset (here you need to make your own header and get some information -same as the one you get from the readContinuousChunks 'info' output)

Careful: when you read a .continuous file, you get a ADbitVolt value: the amount of volts per bit, if you use such a file, you may want to divide the number you extract by the ADbitVolt value before writing it

## Installation
Jut copy them in your matlab 'path'

## Usage
figErwan.m goes to a folder where mat files have been written for each of the mice analized.
it therefore needs you to run ErwanMua.m on the session (open-ephys session using 32 channels)

then FixNoiseOpenEphys should work by itself:
go to the directory where the data is and type
FixNoiseOpenEphys (pwd)

writeContinuous (fName,lfp,info)
is very handy, you feed it the lfp data, the file name (fName finishing with '.contunuous' and the info header°)
and it writes a .continuous file

## Support
Please tell me if there are bugs or if I made a mistake. 

## Authors and acknowledgment
I got inspired by python codes from Dan Denman and Josh Siegle 'OpenEphys' 

## License
These are under MIT licences
